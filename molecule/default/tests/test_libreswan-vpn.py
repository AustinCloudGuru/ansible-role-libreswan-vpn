import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_fail2ban_is_running(host):
    fail2ban_service = host.service('fail2ban')
    assert fail2ban_service.is_running
    assert fail2ban_service.is_enabled


def test_ipsec_is_running(host):
    ipsec_service = host.service('ipsec')
    assert ipsec_service.is_running
    assert ipsec_service.is_enabled


def test_iptables_is_running(host):
    iptables_service = host.service('iptables')
    assert iptables_service.is_running
    assert iptables_service.is_enabled


def test_xl2tpd_is_running(host):
    xl2tpd_service = host.service('xl2tpd')
    assert xl2tpd_service.is_running
    assert xl2tpd_service.is_enabled
